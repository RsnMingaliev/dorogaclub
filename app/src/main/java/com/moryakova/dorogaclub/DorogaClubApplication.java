package com.moryakova.dorogaclub;

import android.app.Application;

import com.moryakova.dorogaclub.model.DBHelper;
import com.moryakova.dorogaclub.model.UserProfile;

public class DorogaClubApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        DBHelper.getInstance(this);
        UserProfile.getInstance(this);
    }
}
