package com.moryakova.dorogaclub.view.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.EditText;

import com.moryakova.dorogaclub.view.activity.BaseActivity;

public class BaseFragment extends Fragment implements View.OnClickListener {
    protected BaseActivity baseActivity;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        baseActivity = (BaseActivity) activity;
    }

    @Override
    public void onClick(View v) {

    }

    protected void showAlert(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(message);
        builder.setCancelable(false);
        builder.setNeutralButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.create().show();
    }

    protected void showAlert(int res) {
        showAlert(getString(res));
    }

    protected static String getText(EditText editText) {
        return editText.getText().toString();
    }
}
