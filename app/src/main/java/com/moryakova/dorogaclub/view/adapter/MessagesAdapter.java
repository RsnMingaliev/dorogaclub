package com.moryakova.dorogaclub.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.moryakova.dorogaclub.R;
import com.moryakova.dorogaclub.model.Message;

import java.util.List;

public class MessagesAdapter extends ArrayAdapter<Message> {
    private Context context;
    private List<Message> messages;

    public MessagesAdapter(Context context, List<Message> messages) {
        super(context, R.layout.list_item_message, messages);
        this.context = context;
        this.messages = messages;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.list_item_message, parent, false);
            holder = new ViewHolder();
            holder.title = (TextView) convertView.findViewById(R.id.message_title);
            holder.description = (TextView) convertView.findViewById(R.id.message_description);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        Message message = messages.get(position);
        holder.title.setText(message.getTitle());
        holder.description.setText(message.getDescription());
        return convertView;
    }

    private class ViewHolder {
        private TextView title;
        private TextView description;
    }
}
