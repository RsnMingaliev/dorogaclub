package com.moryakova.dorogaclub.view.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.moryakova.dorogaclub.R;

/**
 * Created on 17.02.15.
 *
 * @author Ruslan Mingaliev,
 *         Software Center
 */
public class AuthFragment extends BaseFragment {
    private Button registerButton;
    private Button submitButton;

    private EditText emailInput;
    private EditText passwordInput;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_auth, container, false);
        registerButton = (Button) view.findViewById(R.id.auth_register_btn);
        submitButton = (Button) view.findViewById(R.id.auth_submit_btn);

        emailInput = (EditText) view.findViewById(R.id.auth_email);
        passwordInput = (EditText) view.findViewById(R.id.auth_password);

        emailInput.setText("admin");
        passwordInput.setText("admin");

        registerButton.setOnClickListener(this);
        submitButton.setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View v) {
        if (v == registerButton) {
            baseActivity.addFragment(RegisterFragment.class);
        } else if (v == submitButton) {
            auth();
        }
    }

    //TODO: сделать проверку валидоности email-a и авторизацию на сервере
    public void auth() {
        String email = getText(emailInput);
        String password = getText(passwordInput);

        if (email.equals("")) {
            showAlert(R.string.auth_error_empty_email);
            return;
        }

        if (password.equals("")) {
            showAlert(R.string.auth_error_empty_password);
            return;
        }

        if (!email.equals("admin") || !password.equals("admin")) {
            showAlert(R.string.auth_error_no_such_user);
            return;
        }


        baseActivity.addFragment(MainFragment.class);
    }
}
