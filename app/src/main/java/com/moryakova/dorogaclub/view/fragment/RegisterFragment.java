package com.moryakova.dorogaclub.view.fragment;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.moryakova.dorogaclub.R;


public class RegisterFragment extends BaseFragment {
    private Toolbar toolBar;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_register, container, false);
        initToolbar(view);
        return view;
    }

    private void initToolbar(View view) {
        toolBar = (Toolbar) view.findViewById(R.id.toolbar);
        baseActivity.setSupportActionBar(toolBar);
        toolBar.setTitle(R.string.register_title);
        toolBar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                baseActivity.back();
            }
        });
        baseActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }
}
