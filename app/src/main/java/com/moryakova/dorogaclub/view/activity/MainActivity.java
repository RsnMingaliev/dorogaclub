package com.moryakova.dorogaclub.view.activity;

import android.os.Bundle;

import com.moryakova.dorogaclub.R;
import com.moryakova.dorogaclub.view.fragment.AuthFragment;

public class MainActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        addFragment(AuthFragment.class);
    }
}
