package com.moryakova.dorogaclub.view.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import com.moryakova.dorogaclub.R;


public class BaseActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public void addFragment(Class<?> clazz, String tag, Bundle bundle) {
        FragmentManager manager = getSupportFragmentManager();
        Fragment fragment = Fragment.instantiate(getBaseContext(), clazz.getName(), bundle);
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(R.id.fragment_container, fragment, tag);
        transaction.addToBackStack(tag);
        transaction.commit();
    }

    public void addFragment(Class<?> clazz, Bundle bundle) {
        addFragment(clazz, clazz.getSimpleName(), bundle);
    }

    public void addFragment(Class<?> clazz) {
        addFragment(clazz, null);
    }

    public void hideKeyboard() {
        View view = getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)
                    getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    public boolean back() {
        FragmentManager manager = getSupportFragmentManager();
        if (manager.getBackStackEntryCount() > 1) {
            manager.popBackStack();
            return true;
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        if (!back())
            System.exit(0);
    }
}