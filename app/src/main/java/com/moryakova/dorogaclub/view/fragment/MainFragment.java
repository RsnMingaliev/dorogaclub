package com.moryakova.dorogaclub.view.fragment;

import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.moryakova.dorogaclub.R;
import com.moryakova.dorogaclub.model.Message;
import com.moryakova.dorogaclub.view.adapter.MessagesAdapter;

import java.util.List;

public class MainFragment extends BaseFragment {

    private ListView messagesList;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);
        messagesList = (ListView) view.findViewById(R.id.main_messages_list);
        initToolbarAndMenu(view);
        initList();
        return view;
    }

    private void initList() {
        List<Message> restored = new Message().getAllItems();
        if (restored == null)
            return;
        MessagesAdapter adapter = new MessagesAdapter(getActivity(), restored);
        messagesList.setAdapter(adapter);
    }

    private void initToolbarAndMenu(View v) {
        Toolbar toolbar = (Toolbar) v.findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.main_title);

        baseActivity.setSupportActionBar(toolbar);

        DrawerLayout drawerLayout = (DrawerLayout) v.findViewById(R.id.drawer_layout);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(baseActivity, drawerLayout, toolbar, R.string.app_name, R.string.app_name) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }
        };
        toggle.setDrawerIndicatorEnabled(true);
        drawerLayout.setDrawerListener(toggle);
        toggle.syncState();

        String[] menuOptions = getResources().getStringArray(R.array.menu_items_student);

        ListView drawerList = (ListView) v.findViewById(R.id.left_drawer);


        drawerList.setAdapter(new ArrayAdapter<>(getActivity(),
                android.R.layout.simple_list_item_1, menuOptions));
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_main, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

        }
        return true;
    }
}
