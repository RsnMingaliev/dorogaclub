package com.moryakova.dorogaclub.model;

import android.content.ContentValues;
import android.database.Cursor;

import java.util.ArrayList;
import java.util.List;

public class Message extends DatabaseEntity {

    private int id;
    private String title;
    private String description;
    private long timeStamp;

    public static final String TABLE_NAME = "messages";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_TITLE = "title";
    public static final String COLUMN_DESCRIPTION = "description";
    public static final String COLUMN_TIMESTAMP = "time_stamp";

    public static final String CREATE_SCRIPT = "create table " + TABLE_NAME + " (" +
            COLUMN_ID + " integer primary key autoincrement, " +
            COLUMN_TITLE + " text not null, " +
            COLUMN_DESCRIPTION + " text not null, " +
            COLUMN_TIMESTAMP + " integer not null);";

    private Message(int id, String title, String description, long timeStamp) {
        super(TABLE_NAME);
        this.id = id;
        this.title = title;
        this.description = description;
        this.timeStamp = timeStamp;
    }

    public Message(String title, String description, long timeStamp) {
        super(TABLE_NAME);
        this.title = title;
        this.description = description;
        this.timeStamp = timeStamp;
    }

    public Message() {
        super(TABLE_NAME);
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public long getTimeStamp() {
        return timeStamp;
    }

    public List<Message> getAllItems() {
        Cursor c = readableDatabase().query(TABLE_NAME, getAllColumns(), null, null, null, null, null);
        if (!c.moveToFirst()) {
            c.close();
            return null;
        }
        List<Message> list = new ArrayList<>();
        while (!c.isAfterLast()) {
            Message item = (Message) fromCursorToEntity(c);
            list.add(item);
            c.moveToNext();
        }
        c.close();
        return list;
    }

    @Override
    public String[] getAllColumns() {
        return new String[]{COLUMN_ID, COLUMN_TITLE, COLUMN_DESCRIPTION, COLUMN_TIMESTAMP};
    }

    @Override
    public DatabaseEntity fromCursorToEntity(Cursor c) {
        int id = c.getInt(0);
        String title = c.getString(1);
        String description = c.getString(2);
        long timeStamp = c.getLong(3);

        return new Message(id, title, description, timeStamp);
    }

    @Override
    public ContentValues generateContentValues() {
        ContentValues values = new ContentValues();
        values.put(COLUMN_TITLE, title);
        values.put(COLUMN_DESCRIPTION, description);
        values.put(COLUMN_TIMESTAMP, timeStamp);
        return values;
    }
}
