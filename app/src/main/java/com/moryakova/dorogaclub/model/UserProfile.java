package com.moryakova.dorogaclub.model;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created on 11.03.15.
 *
 * @author Ruslan Mingaliev,
 *         Software Center
 */
public class UserProfile {
    private static UserProfile instance;

    public static UserProfile getInstance() {
        return instance;
    }

    public static UserProfile getInstance(Context context) {
        if (instance == null)
            instance = new UserProfile(context);
        return instance;
    }


    private SharedPreferences user;

    private UserProfile(Context context) {
        user = context.getSharedPreferences("user", Context.MODE_PRIVATE);
    }

    public void setName(String name) {
        user.edit().putString("name", name);
    }

    public String getName() {
        return user.getString("name", null);
    }
}
