package com.moryakova.dorogaclub.model;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public abstract class DatabaseEntity {
    private String tableName;


    public DatabaseEntity getItemWithId(int id) {
        Cursor c = readableDatabase().query(tableName, getAllColumns(),
                "_id = ?", new String[]{String.valueOf(id)}, null, null, null);
        if (!c.moveToFirst()) {
            c.close();
            return null;
        }

        DatabaseEntity entity = fromCursorToEntity(c);
        c.close();
        return entity;
    }

    public long saveItem() {
        SQLiteDatabase writableDatabase = writableDatabase();
        ContentValues values = generateContentValues();
        writableDatabase.beginTransaction();
        long insertId = writableDatabase.insert(tableName, null, values);
        writableDatabase.setTransactionSuccessful();
        writableDatabase.endTransaction();
        return insertId;
    }

    protected SQLiteDatabase writableDatabase() {
        return DBHelper.getInstance().getWritableDatabase();
    }

    protected SQLiteDatabase readableDatabase() {
        return DBHelper.getInstance().getReadableDatabase();
    }

    public DatabaseEntity(String tableName) {
        this.tableName = tableName;
    }

    public abstract String[] getAllColumns();

    public abstract DatabaseEntity fromCursorToEntity(Cursor c);

    public abstract ContentValues generateContentValues();
}
